
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class LoginPage {
    private static ChromeDriver driver;

    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Marcin\\Downloads\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/index.xhtml");

        // Testy funkcjonalności logowania
        loginAdminAccount_Successfull();
        loginAdminAccount_EmptyUserName_Failed();
        loginAdminAccount_EmptyPassword_Failed();
        loginAdminAccount_EmptyUserNameAndPassword_Failed();
        loginAdminAccount_WrongUserName_Failed();
        loginAdminAccount_WrongPassword_Failed();
        loginAdminAccount_WrongUserNameAndPassword_Failed();

        // Testy bezpieczeństwa
        loginSQLInjection_AlwaysTrue1_Failed();
        loginSQLInjection_AlwaysTrue2_Failed();
        loginSQLInjection_ServerBlock_Failed();
        loginSQLInjection_DropDatabase_Failed();
    }

    private static void loginAdminAccount_Successfull(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.findElement(By.id("logutForm")).isDisplayed());
        System.out.println("Logowanie zakończone sukcesem");
        WebElement logOut = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut.click();
    }

    private static void loginAdminAccount_EmptyUserName_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"loginForm\"]")).getText().toLowerCase().contains("loginform:login: validation error"));
        System.out.println("Poprawnie wykryto brak loginu");
    }

    private static void loginAdminAccount_EmptyPassword_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"loginForm\"]")).getText().toLowerCase().contains("loginform:password: validation error"));
        System.out.println("Poprawnie wykryto brak hasła");
    }

    private static void loginAdminAccount_EmptyUserNameAndPassword_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"loginForm\"]")).getText().toLowerCase().contains("loginform:login: validation error") &&
                driver.findElement(By.xpath("//*[@id=\"loginForm\"]")).getText().toLowerCase().contains("loginform:password: validation error"));
        System.out.println("Poprawnie wykryto brak danych wejściowych");
    }

    private static void loginAdminAccount_WrongUserName_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin1");
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie wykryto złe dane (username) i niezalogowano");
    }

    private static void loginAdminAccount_WrongPassword_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        passwordInput.sendKeys("admin1");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie wykryto złe dane (password) i niezalogowano");
    }

    private static void loginAdminAccount_WrongUserNameAndPassword_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin1");
        passwordInput.sendKeys("admin1");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie wykryto złe dane (username i password) i niezalogowano");
    }

    private static void loginSQLInjection_AlwaysTrue1_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("105 OR 1=1");
        passwordInput.sendKeys("105 OR 1=1");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie niezalogowano za pomocą SQL Injection (105 OR 1=1)");
    }

    private static void loginSQLInjection_AlwaysTrue2_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("\" or \"\"=\"");
        passwordInput.sendKeys("\" or \"\"=\"");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie niezalogowano za pomocą SQL Injection (\" or \"\"=\")");
    }

    private static void loginSQLInjection_ServerBlock_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys(" x' AND BENCHMARK(9999999,BENCHMARK(999999,BENCHMARK(999999,MD5(NOW()))))=0 OR '1'='1");
        passwordInput.sendKeys("xx");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie niezablokowano serwera za pomocą blokowania serwera");
    }

    private static void loginSQLInjection_DropDatabase_Failed(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("105; DROP TABLE Users");
        passwordInput.sendKeys("xx");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();
        WebElement userNameInput1=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput1 = driver.findElement(By.id("loginForm:password"));
        userNameInput1.clear();
        passwordInput1.clear();
        WebElement button1 = driver.findElement(By.id("loginForm:button"));
        userNameInput1.sendKeys("admin");
        passwordInput1.sendKeys("admin");
        button1 = driver.findElement(By.id("loginForm:button"));
        button1.click();
        WebElement logOut = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut.click();
        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie niezablokowano serwera za pomocą blokowania serwera");
    }
}