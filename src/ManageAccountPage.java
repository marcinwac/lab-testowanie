
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class ManageAccountPage {
    private static ChromeDriver driver;

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Marcin\\Downloads\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/index.xhtml");

        // Testy funkcjonalności zarządzania kontem
        changeAdminPassword_Successfull();
        changeAdminPassword_WrongPassword_Unsuccessfull();
        changeAdminPassword_OldPassword_Unsuccessfull();
    }

    private static void changeAdminPassword_Successfull(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();

        WebElement manageAccount = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount.click();

        WebElement newPasswordInput = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput.sendKeys("admin5");
        WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton.click();
        WebElement backButton = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton.click();

        WebElement logOut = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut.click();

        WebElement userNameInput1 =driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput1 = driver.findElement(By.id("loginForm:password"));
        userNameInput1.sendKeys("admin");
        passwordInput1.sendKeys("admin5");
        WebElement loginButton = driver.findElement(By.id("loginForm:button"));
        loginButton.click();

        Assert.assertTrue(driver.findElement(By.id("logutForm")).isDisplayed());
        System.out.println("Logowanie zakończone sukcesem");

        WebElement manageAccount2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount2.click();

        WebElement newPasswordInput2 = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput2.sendKeys("admin");
        WebElement saveButton2 = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton2.click();
        WebElement backButton2 = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton2.click();

        WebElement logOut2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut2.click();
        System.out.println("Poprawnie przywrócono hasło");
    }

    private static void changeAdminPassword_WrongPassword_Unsuccessfull(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();

        WebElement manageAccount = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount.click();

        WebElement newPasswordInput = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput.sendKeys("admin5");
        WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton.click();
        WebElement backButton = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton.click();

        WebElement logOut = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut.click();

        WebElement userNameInput1 =driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput1 = driver.findElement(By.id("loginForm:password"));
        userNameInput1.sendKeys("admin");
        passwordInput1.sendKeys("admin7");
        WebElement loginButton = driver.findElement(By.id("loginForm:button"));
        loginButton.click();

        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie nie udało się zalogować złym hasłem");

        WebElement userNameInput2 =driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput2 = driver.findElement(By.id("loginForm:password"));
        userNameInput2.clear();
        passwordInput2.clear();
        userNameInput2.sendKeys("admin");
        passwordInput2.sendKeys("admin5");
        WebElement loginButton2 = driver.findElement(By.id("loginForm:button"));
        loginButton2.click();

        WebElement manageAccount2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount2.click();

        WebElement newPasswordInput2 = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput2.sendKeys("admin");
        WebElement saveButton2 = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton2.click();
        WebElement backButton2 = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton2.click();

        WebElement logOut2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut2.click();
        System.out.println("Poprawnie przywrócono hasło");
    }

    private static void changeAdminPassword_OldPassword_Unsuccessfull(){
        WebElement userNameInput=driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput = driver.findElement(By.id("loginForm:password"));
        userNameInput.clear();
        passwordInput.clear();
        userNameInput.sendKeys("admin");
        passwordInput.sendKeys("admin");
        WebElement button = driver.findElement(By.id("loginForm:button"));
        button.click();

        WebElement manageAccount = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount.click();

        WebElement newPasswordInput = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput.sendKeys("admin5");
        WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton.click();
        WebElement backButton = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton.click();

        WebElement logOut = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut.click();

        WebElement userNameInput1 =driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput1 = driver.findElement(By.id("loginForm:password"));
        userNameInput1.sendKeys("admin");
        passwordInput1.sendKeys("admin");
        WebElement loginButton = driver.findElement(By.id("loginForm:button"));
        loginButton.click();

        Assert.assertTrue(driver.getCurrentUrl().contains("http://localhost:8080/index.xhtml") || driver.getCurrentUrl().contains("http://localhost:8080/main.xhtml"));
        System.out.println("Poprawnie nie udało się zalogować starym hasłem");

        WebElement userNameInput2 =driver.findElement(By.id("loginForm:login"));
        WebElement passwordInput2 = driver.findElement(By.id("loginForm:password"));
        userNameInput2.clear();
        passwordInput2.clear();
        userNameInput2.sendKeys("admin");
        passwordInput2.sendKeys("admin5");
        WebElement loginButton2 = driver.findElement(By.id("loginForm:button"));
        loginButton2.click();

        WebElement manageAccount2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[2]"));
        manageAccount2.click();

        WebElement newPasswordInput2 = driver.findElement(By.xpath("//*[@id=\"passwordForm:passwordTable:0:password\"]"));
        newPasswordInput2.sendKeys("admin");
        WebElement saveButton2 = driver.findElement(By.xpath("//*[@id=\"passwordForm\"]/input[2]"));
        saveButton2.click();
        WebElement backButton2 = driver.findElement(By.xpath("//*[@id=\"j_idt6\"]/a"));
        backButton2.click();

        WebElement logOut2 = driver.findElement(By.xpath("//*[@id=\"logoutForm\"]/a[1]"));
        logOut2.click();
        System.out.println("Poprawnie przywrócono hasło");
    }
}